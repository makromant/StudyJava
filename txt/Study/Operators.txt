Selection operators:
	# IF
	if (condition)
		operator1; 
	else 
		operator2;

	if (condition1) 
		operator1;
	else if (conditon2)
		operator2;

	# SWITCH
	switch (expression) {
		case value1:
			// operators1
			break; // necessary! 
		case value2:
			// operators2
			break; // necessary!
		...
		case valueN:
			// operatorsN
			break; // necessary!
		default:
			// default operations; 
			/**
			* If the values ​​of none of the constants in the statements of the branches of the 
			* case do not coincide with the value of the expression, then the operator in the 
			* default branch is executed.
			*/
	}

	# Embedded SWITCH
	switch (expression) {
		case value1:
			switch (embeddedExpression){
				case eValue1:
					// eOperators1
					break; // necessary!
				case eValue2:
					//eOperators2
					break; // necessary!
			}
			break; // necessary!
		case value2: // ...
	}

Cycle operators:
	# WHILE : check condition -> execute cycle
	while(condition){
		// body of the cycle
	}

	# DO WHILE : execute cycle -> check condition
	do {
		// body of the cycle
	} while (condition)

	# FOR 
	for (initialization; condition; iteration){
		// body of the cycle
	}
	# FOR EACH
	for (type iteration value : collection) block of operators;

Jump operators:
	break; - immediate completion of the cycle
	# BREAK like GOTO :
		break label; - control is passed to the code block labeled with a label
		example:
			...
			first: { // first - label
				second: { // second - label
					break first; // 
				}
			}
	continue; - transition in the body of the cycle to its end
	return; - explicit exit from the method